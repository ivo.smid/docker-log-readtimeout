package org.example;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;

@Testcontainers
class MainTest {
    private static final Logger logger = LoggerFactory.getLogger(MainTest.class);

    private static final int waitMinutes = 4;

    @Container
    public GenericContainer<?> container = new GenericContainer<>(DockerImageName.parse("alpine:latest"))
            .withLogConsumer(new Slf4jLogConsumer(logger)
                    .withPrefix("***"))
            .withCommand("sh", "-c", "echo 'wait 10s' && sleep 10s && echo 'wait " + waitMinutes + "m' && sleep " + waitMinutes + "m && echo 'started'")
            .waitingFor(new LogMessageWaitStrategy()
                    .withRegEx("started\\n")
                    .withStartupTimeout(Duration.ofMinutes(waitMinutes + 1)));

    @Test
    void name() {
        logger.info("end ***");
    }
}
